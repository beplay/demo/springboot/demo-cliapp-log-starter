package th.co.truecorp.springboot.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class DemoCliappLogStarterApplication implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(DemoCliappLogStarterApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
    log.info("Hello world");
    log.debug("Hello debug world");
	}

}
